package se325.lecture02.basicgreetings.server;

import se325.lecture02.basicgreetings.shared.GreetingService;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class GreetingServiceServant extends UnicastRemoteObject implements GreetingService {

    public GreetingServiceServant() throws RemoteException {
    }

    @Override
    public String getGreeting(String name) throws RemoteException {
        return "Hello, " + name + "!";
    }
}
package se325.lecture02.advancedgreetings.shared;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface GreetingServiceFactory extends Remote {

    GreetingService getGreetingService(String language) throws RemoteException;

}
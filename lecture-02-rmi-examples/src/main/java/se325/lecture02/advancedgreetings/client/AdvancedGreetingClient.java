package se325.lecture02.advancedgreetings.client;

import se325.lecture02.Keyboard;
import se325.lecture02.advancedgreetings.shared.Greeting;
import se325.lecture02.advancedgreetings.shared.GreetingService;
import se325.lecture02.advancedgreetings.shared.GreetingServiceFactory;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class AdvancedGreetingClient {

    public static void main(String[] args) throws RemoteException, NotBoundException {

        Registry lookupService = LocateRegistry.getRegistry("localhost", 8080);

        GreetingServiceFactory greetingFactoryProxy = (GreetingServiceFactory) lookupService.lookup("greetingFactory");

        String language = Keyboard.prompt("What language do you want to use?");

        GreetingService greetingServiceProxy = greetingFactoryProxy.getGreetingService(language);

        String name = Keyboard.prompt("What is your name?");

        Greeting greeting = greetingServiceProxy.getGreeting(name);

        System.out.println("Your greeting: \"" + greeting + "\"");
    }

}

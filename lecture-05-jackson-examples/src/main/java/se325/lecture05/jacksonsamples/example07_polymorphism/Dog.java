package se325.lecture05.jacksonsamples.example07_polymorphism;

import com.fasterxml.jackson.annotation.JsonTypeName;

public class Dog extends Animal {

    public Dog() {
        this(null);
    }

    public Dog(String name) {
        super("Dog", name);
    }

    @Override
    public void sayHello() {
        System.out.println(name + " says woof!");
    }
}

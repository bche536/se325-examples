package se325.lecture05.jacksonsamples.services;

import se325.lecture05.jacksonsamples.example01_basic.Book;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/hello")
public class HelloJacksonResource {

    @POST
    @Path("/book")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Book echoBook(Book book) {

        return book;

    }
}